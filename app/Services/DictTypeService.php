<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2021 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace App\Services;

use App\Models\DictTypeModel;

/**
 * 字典类型-服务类
 * @author 牧羊人
 * @since 2020/11/11
 * Class DictTypeService
 * @package App\Services
 */
class DictTypeService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2020/11/11
     * DictTypeService constructor.
     */
    public function __construct()
    {
        $this->model = new DictTypeModel();
    }
}
