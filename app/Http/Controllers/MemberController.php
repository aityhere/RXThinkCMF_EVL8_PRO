<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2021 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace App\Http\Controllers;

use App\Services\MemberService;

/**
 * 会员管理-控制器
 * @author 牧羊人
 * @since 2020/11/11
 * Class MemberController
 * @package App\Http\Controllers
 */
class MemberController extends Backend
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2020/11/11
     * MemberController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->service = new MemberService();
    }
}
